import React from 'react';

const AboutMe = () => {
  return (
    <div style={{
      'background-color': 'white',
      'border-top':'3px solid orange',
      'color':'#005980',
      'font-size':'16px',
      'margin':'40px auto 0px auto',
      'padding':'2.5% 0px',
      'text-align':'center',
      'width':'98%'
    }}>
        <div style={{
          'font-weight':'bold',
          'font-size':'17px',
        }}>
          Biografi <span style={{'background-color':'orange'}}>{" "}
          Creator</span>
        </div>
        <div style={{'margin-top':'10px'}}>
          <b>Mochamad Rafli Ramadhan</b>, 2021
        </div>        
    </div>
  )
}

export default AboutMe;