import React from "react";
import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import AppBar from './component/AppBar';

class App extends React.Component {
  render() {
    return (
      <div>
        <AppBar/>
      </div>
    );
  }
}
export default App;
