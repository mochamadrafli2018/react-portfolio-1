import React from "react";
import Footer from "./Footer";
import RoutingContent from "../content-component/RoutingContent";
import {
  FetchDataGit,
  FetchDataLaravel, 
  FetchDataNodeJs,
  FetchDataReact,
  FetchDataVue
} from '../content-component/FetchingApi';

const MainContent = () => {

  return (
    <div class='bold MainContent' 
    style={{
      'border':'',
      "font-size": "16px",
      'margin':'-60px auto 0px auto',
      'padding':'15px 0px',
      'background-color':'#f5fcff',
      width: "100%",
    }}>
        <h3 class="bg-0b96b5"
        style={{
          'background-color': "#005980",
          'border': '1px solid white',
          'box-shadow':'0px 0px 7px 2px #005980',
          'color': 'white',
          'margin': '0 auto',
          'padding':'0',
          'text-align':'center',
          'width': '235px',
        }}>
          CatatanCoding
        </h3>
        <p class='text-center' style={{
          color:'#005980',
          'font-weight':'bold',
          margin: "10px auto",
          width: "98%",
        }}>
          "Berbagai sumber referensi seputar pemrograman web"
        </p>
        <h5 class="bold text-center" 
        style={{
          margin: "0px auto",
          padding: "auto",
          width: "98%",
        }}>
          <b>Rekomendasi </b>
          <span style={{'color':'#005980'}}><b>website</b></span>
        </h5>
        <p class="bold text-center" 
        style={{
          margin: "0 auto",
          padding: "auto",
          width: "98%",
        }}>
          Berdasarkan alur belajar
        </p>
        <RoutingContent/>
        <Footer />
    </div>
  );
};

export default MainContent;