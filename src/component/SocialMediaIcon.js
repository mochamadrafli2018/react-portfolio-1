import React from 'react';
import GitHubIcon from "@material-ui/icons/GitHub";
import InstagramIcon from "@material-ui/icons/Instagram";
import LinkedInIcon from "@material-ui/icons/LinkedIn";
import MailIcon from "@material-ui/icons/Mail";

const SocialMedia = () => {
    return(
      <ul
        style={{
          'background-color': '#f5fcff',
          border: "3px solid #005980",
          "border-radius": "5px",
          display: "inline-block",
          margin: "0px",
          padding: "0% 0%",
          position: "fixed",
          left: "0%",
          "list-style-type": "none",
          bottom: "30%",
          width: "33px",
        }}
      >
        <li
          style={{
            margin: "20px auto",
            padding: "0px",
          }}
        >
          <a
            href="https://www.github.com/mochamadrafli2018"
            style={{
              color: "#005980",
            }}
          >
            <GitHubIcon style={{ height: "22px" }} />
          </a>
        </li>
        <li
          style={{
            margin: "20px auto",
            padding: "0px",
          }}
        >
          <a
            href="https://www.linkedin.com/in/mochamad-rafli-ramadhan/"
            style={{
              color: "#005980",
            }}
          >
            <LinkedInIcon style={{ height: "27px" }} />              
          </a>
        </li>
        <li
          style={{
            margin: "20px auto",
            padding: "0px",
          }}
        >
          <a
            href="https://www.instagram/rafli.r.rmdhn"
            style={{
              color: "#005980",
            }}
          >
            <InstagramIcon style={{ height: "25px" }} />
          </a>
        </li>
        <li
          style={{
            margin: "20px auto",
            padding: "0px",
          }}
        >
          <a
            href="https://mail.google.com/mail/?view=cm&fs=1&to=mochamadrafli2018@mail.ugm.ac.id"
            style={{
              color: "#005980",
            }}
          >
            <MailIcon style={{ height: "27px" }} />
          </a>
        </li>
      </ul>
    )
}
export default SocialMedia;