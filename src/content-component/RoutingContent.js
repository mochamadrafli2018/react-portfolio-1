import React from "react"
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import {
  FetchDataGit,
  FetchDataLaravel, 
  FetchDataNodeJs,
  FetchDataReact,
  FetchDataVue
} from './FetchingApi';
import './RoutingContent.css';

const RoutingData1 = [
  {
    'route':'/',
    'text':'Laravel'
  },
  {
    'route':'/git',
    'text':'Git'
  },
  {
    'route':'/react',
    'text':'React'
  }
];
const RoutingData2 = [
  {
    'route':'/nodejs',
    'text':'NodeJs'
  },
  {
    'route':'/vue',
    'text':'Vue'
  }
];

const Routing = ({ RoutingData }) => {
  return(
    <ul class ='text-center' style={{
      'list-style-type':'none',
      'margin':'5px auto',
      'padding':'5px',
      'width':'98%'
    }}>
      {RoutingData.map((i) => (
        <li class='link' style={{
          'background-color':'',
          'border':'',
          'border-radius':'3px',
          'display':'inline',
          'margin':'0px 1%',
          'padding':'3px 1px'
        }}>
          <Link class='link-2' to={i.route} 
          style={{
            'background-color':'white',
            'border':'',
            'border-radius':'3px',
            'box-shadow':'0px 0px 7px 0px #005980',
            'color':'#005980',
            'font-weight':'bold',
            'padding':'2px',
            'text-decoration':'none'
          }}>
            {i.text}
          </Link>
        </li>
      ))}
    </ul>
  );
}

const StaticRouting = () => {
  return (
    <>
      <Router>
        <div style={{
          'margin':'0px'
        }}>
          <Routing RoutingData={RoutingData1}/>
          <Routing RoutingData={RoutingData2}/>
        </div>
        <Switch>
          <Route exact path="/" >     
            <FetchDataLaravel/>
          </Route>
          <Route exact path="/git">
            <FetchDataGit/>
          </Route>
          <Route exact path="/nodejs">
            <FetchDataNodeJs/>
          </Route>
          <Route exact path="/react">
            <FetchDataReact/>
          </Route>
          <Route exact path="/vue">
            <FetchDataVue/>
          </Route>
        </Switch>
      </Router>
    </>
  );
};
export default StaticRouting;