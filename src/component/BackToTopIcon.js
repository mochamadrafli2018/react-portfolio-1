import React from "react";
import MainContent from './MainContent';
// Material UI
import PropTypes from "prop-types";
import Toolbar from "@material-ui/core/Toolbar";
import { makeStyles } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import useScrollTrigger from "@material-ui/core/useScrollTrigger";
import Fab from "@material-ui/core/Fab";
import KeyboardArrowUpIcon from "@material-ui/icons/KeyboardArrowUp";
import Zoom from "@material-ui/core/Zoom";

// Don't change this code! this code was for back to the top icon
const useStyles = makeStyles((theme) => ({
  root: {
    position: "fixed",
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
}));

function ScrollTop(props) {
  const { children, window } = props;
  const classes = useStyles();
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({
    target: window ? window() : undefined,
    disableHysteresis: true,
    threshold: 100,
  });

  const handleClick = (event) => {
    const anchor = (event.target.ownerDocument || document).querySelector(
      "#back-to-top-anchor"
    );

    if (anchor) {
      anchor.scrollIntoView({ behavior: "smooth", block: "center" });
    }
  };

  return (
    <Zoom in={trigger}>
      <div 
      onClick={handleClick} 
      role="presentation" 
      className={classes.root}
      style={{'background-color':''}}>
        {children}
      </div>
    </Zoom>
  );
}

ScrollTop.propTypes = {
  children: PropTypes.element.isRequired,
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

// BackToTop Icon
export default function BackToTop(props) {
  return (
    <React.Fragment style={{'margin-top':'0px'}}>
      <CssBaseline />
      {/*
      <AppBar>
        <NavBar />
      </AppBar>
      */}
      <Toolbar id="back-to-top-anchor" />
      {/*Main Content from Header to Footer */}
      <MainContent style={{'margin-top':'0px'}}/>
      {/*Icon Back to The Top*/}
      <ScrollTop {...props}>
        <Fab
          color=""
          size="small"
          aria-label="scroll back to top"
          style={{ "background-color": "orange" }}
        >
          <KeyboardArrowUpIcon />
        </Fab>
      </ScrollTop>
    </React.Fragment>
  );
}
