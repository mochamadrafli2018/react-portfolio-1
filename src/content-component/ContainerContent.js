import React from 'react';
import { Spinner } from 'react-bootstrap'
import Cardbox from './Card';

const ContainerContent = ({api, isLoading, isError}) => {
  return (
    <div>
      {isError ? (
        <div class='text-center' style={{
          'color':'#005980',
          'margin-top':'30px'
        }}> 
          Ups, sepertinya koneksi internet kamu tidak aktif... ...
        </div> 
      ) : (
    <div>
      {isLoading ? (
        <div class='text-center' style={{
          'color':'#005980',
          'margin-top':'30px'
        }}>
          <Spinner 
            animation="border" variant="#005980" 
            style={{ height: '18px', 'width': '18px' }}
          />
          Tunggu, Data akan Ditampilkan...
        </div>
      ) : (
    <div style={{
      'border':'3px solid orange',
      'border-radius':'5px',
      'background-color':'#f5fcff',
      'margin':'0.5% auto',
      'padding':'2% auto',
      'width':'98%'
    }}>
      {api.map((i) => (
        <Cardbox i={i} key={i.id} />
      ))}
    </div>  
          )
      }
    </div>
          )
      }
    </div>
  )
}
export default ContainerContent;