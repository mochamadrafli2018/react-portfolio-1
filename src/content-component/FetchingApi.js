import { useEffect, useState } from 'react';
import ContainerContent from './ContainerContent';

const FetchDataGit = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [api, setApi] = useState([]);
  const [isError, setIsError] = useState(false);
  const url = "https://hapi-nested-api.herokuapp.com/git"

  useEffect( () => {
    fetch(url)
      .then((res) => {return res.json()})
      .then((data) => {
        // if Rest API success, save data from response to local state
        // if JSON start with {"laravel":[{.., write setApi(data.laravel)
        // if JSON start with [{..., write setApi(data)
        setApi(data.datas);
        setIsLoading(false);
      })
      .catch((error) => {
        setIsError(true);
      });
      
  }, []);

  return (
    <ContainerContent api={api} isLoading={isLoading} isError={isError}/>
  );
}

const FetchDataLaravel = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [api, setApi] = useState([]);
  const [isError, setIsError] = useState(false);
  const url = "https://hapi-nested-api.herokuapp.com/laravel"

  useEffect( () => {
    fetch(url)
      .then((res) => {return res.json()})
      .then((data) => {
        // if Rest API success, save data from response to local state
        // if JSON start with {"laravel":[{.., write setApi(data.laravel)
        // if JSON start with [{..., write setApi(data)
        setApi(data.datas);
        setIsLoading(false);
      })
      .catch((error) => {
        setIsError(true);
      });
  }, []);

  return (
    <ContainerContent api={api} isLoading={isLoading} isError={isError}/>
  );
}

const FetchDataNodeJs = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [api, setApi] = useState([]);
  const [isError, setIsError] = useState(false);
  const url = "https://hapi-nested-api.herokuapp.com/nodejs"

  useEffect( () => {
    fetch(url)
      .then((res) => {return res.json()})
      .then((data) => {
        // if Rest API success, save data from response to local state
        // if JSON start with {"laravel":[{.., write setApi(data.laravel)
        // if JSON start with [{..., write setApi(data)
        setApi(data.datas);
        setIsLoading(false);
      })
      .catch((error) => {
        setIsError(true);
      });
  }, []);

  return (
    <ContainerContent api={api} isLoading={isLoading} isError={isError}/>
  );
}

const FetchDataReact = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [api, setApi] = useState([]);
  const [isError, setIsError] = useState(false);
  const url = "https://hapi-nested-api.herokuapp.com/react"

  useEffect( () => {
    fetch(url)
      .then((res) => {return res.json()})
      .then((data) => {
        // if Rest API success, save data from response to local state
        // if JSON start with {"laravel":[{.., write setApi(data.laravel)
        // if JSON start with [{..., write setApi(data)
        setApi(data.datas);
        setIsLoading(false);
      })
      .catch((error) => {
        setIsError(true);
      });
  }, []);

  return (
    <ContainerContent api={api} isLoading={isLoading} isError={isError}/>
  );
}

const FetchDataVue = () => {
  const [isLoading, setIsLoading] = useState(true);
  const [api, setApi] = useState([]);
  const [isError, setIsError] = useState(false);
  const url = "https://hapi-nested-api.herokuapp.com/vue"

  useEffect( () => {
    fetch(url)
      .then((res) => {return res.json()})
      .then((data) => {
        // if Rest API success, save data from response to local state
        // if JSON start with {"laravel":[{.., write setApi(data.laravel)
        // if JSON start with [{..., write setApi(data)
        setApi(data.datas);
        setIsLoading(false);
      })
      .catch((error) => {
        setIsError(true);
      });
  }, []);

  return (
    <ContainerContent api={api} isLoading={isLoading} isError={isError}/>
  );
}

export {
  FetchDataGit,
  FetchDataLaravel,
  FetchDataNodeJs,
  FetchDataReact,
  FetchDataVue
}; 
