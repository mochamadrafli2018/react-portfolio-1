import React from "react";
import FavoriteIcon from "@material-ui/icons/Favorite";

const Footer = () => {
  return (
    <footer style={{
      "background-color": "#005980",
      "border-top": "3px solid orange",
      color: "white",
      "font-size": "15px",
      margin: "40px auto 0 auto",
      padding: "1.5% 0",
      "text-align": "center",
      width: "98%",
    }}>
      <div style={{'margin': '0'}}>
        Punya saran dan kritik terkait{" "}
        <span style={{ "background-color": "orange", 'color': 'black' }}>
        UI/UX
        </span> website ini ?
        
      </div>
      <div style={{margin: '0'}}>
        Kamu bisa klik{" "}
        <span style={{ "background-color": "orange", 'color': 'black' }}>
        form
        </span> dibawah ini :
      </div>
      <a href="https://forms.gle/v5Jn9cFSr58ccqfH9"
        style={{
          color: "orange",
          margin: "0",
          padding: "0",
      }}>
        forms.gle/v5Jn9cFSr58ccqfH9
      </a>
      <div style={{ "margin": "0" }}>
          &#169; <span style={{}}>
          Mochamad Rafli Ramadhan
          </span>, 2021
      </div>
      <div style={{ "margin": "0" }}>
        Dibuat dengan{" "}
        <FavoriteIcon style={{
          color: "orange", 
          'height':' 20px'
        }}/>{" "}
        di IDN
      </div>
    </footer>
  );
};
export default Footer;
