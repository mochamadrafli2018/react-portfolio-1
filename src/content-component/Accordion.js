import React from 'react';
import { Accordion } from 'react-bootstrap';
import MediaCard from './Media';
import { Row } from 'react-bootstrap';
const Accord = ({ i }) => {
  const dataCard = i.card;
  return( 
    <Accordion.Item eventKey={i.eventKey}>
      <Accordion.Header style={{
        'border-top':'2px solid #005980',
        'color':'#005980',
        'font-weight':'bold'
      }}>
        <div style={{
          'color':'#005980',
          'font-weight':'bold'
        }}>{i.item}</div>
      </Accordion.Header>
      <Accordion.Body style={{
        'margin':'0px',
        'padding':'0px'
      }}>
        <Row style={{
          'margin':'3px',
          'padding':'0px'
        }}>
          {dataCard.map((i) => (
              <MediaCard i={i}/>
          ))}
        </Row>
      </Accordion.Body>
    </Accordion.Item>
  );
}
export default Accord;