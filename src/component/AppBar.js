import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import { Nav, Navbar } from 'react-bootstrap';
import AboutMe from './AboutMe';
import BackToTop from './BackToTopIcon';
import SocialMediaIcon from './SocialMediaIcon';

// AppBar
const AppBar = () => {
    const Routes = [
      {
        link:'/',
        title:'Home'
      },
      {
        link:'/aboutme',
        title:'Creator'
      }
    ]
  
    return (
      <div class="poppins" style={{
        'background-color':'white',
        'color':'',
        'font-size':'16px',
        'margin':'0px',
        'padding':'0px',
      }}>
        <Router style={{
          'border-bottom':'',
          'margin':'0px',
          'padding':'0px',
        }}>
          <Navbar variant="dark" class='' style={{
          'border-bottom':'3px solid orange',
          'margin':'0px auto',
          'padding':'0px 0px',
          'position':'',
          'top':'0',
          'width':'98%'
          }}>
            <Nav className="mr-auto" style={{
              'border': '',
              'margin':'0px auto',
              'padding':'0px'
            }}>
              {Routes.map((i) => (
                  <Link to={i.link} 
                  className='navlink' 
                  style={{
                    'border-bottom':'2px solid orange',
                    'color':'#005980',
                    'margin':'5px',
                    'padding':'0px',
                    'text-decoration':'none'
                  }}>
                    <div class='nav-item'><b>{i.title}</b></div>
                  </Link>
              ))}
            </Nav>
          </Navbar>
          <Switch>
            <Route exact path="/"><BackToTop/></Route>
            <Route exact path="/aboutme"><AboutMe/></Route>
          </Switch>          
        </Router>
        <SocialMediaIcon/>
      </div>
    )
}
export default AppBar;