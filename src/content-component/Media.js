import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
  },
  media: {
    height: 140,
  },
});

export default function MediaCard({ i }) {
  const classes = useStyles();

  return (
    <Card className={classes.root} class='quicksand' style={{
      'background-color':'orange',
      'border':'2px solid #005980',
      'border-radius':'3px',
      'box-shadow':'0px 0px 7px 0px #c4c4c4',
      'height':'auto',
      'margin':'3px',
      'padding':'0px',
      'width':'220px'
    }}>
      <CardContent style={{}}>
        <Typography gutterBottom variant="h5" component="h2"
        style={{'color':'#005980'}}>
          <h4>{i.cardtitle}</h4>
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p"
        style={{'color':'#005980'}}>
          Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
          across all continents except Antarctica
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small" color="black">
          <p style={{'color':'white'}}>
            <b>Share</b>
          </p>
        </Button>
        <a href={i.link} style={{'text-decoration':'none'}}>
          <Button size="small" color="primary">
            <p style={{'color':'#005980'}}>
              <b>Learn More</b>
            </p>
          </Button>
        </a>
      </CardActions>
    </Card>
  );
}