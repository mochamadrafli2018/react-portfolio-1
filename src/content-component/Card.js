import React from 'react';
import { Accordion, Card } from 'react-bootstrap';
import Accord from './Accordion';
import '../App.css';
const Cardbox = ({ i }) => {
  const dataAccordion = i.accordion;
  return(
    <>
      <Card style={{
        'border':'2px solid #005980',
        'border-top':'5px solid #005980',
        'border-radius':'5px',
        'box-shadow':'0px 0px 4px 0px #005980',
        'font-size': '16px',
        'margin':'15px auto',
        'max-width':'850px',
        'padding':'auto',
        'width':'90%'
      }}>
        <Card.Body>
          <Card.Title>
            {i.title}
          </Card.Title>
          <Card.Text>
            {i.review}
          </Card.Text>
          <Accordion defaultActiveKey="0">
            {dataAccordion.map((i) => (   
              <Accord i={i}/>
            ))}
          </Accordion>
        </Card.Body>
      </Card>
    </>
  );
}
export default Cardbox;